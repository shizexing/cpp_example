import os
import platform

def generate():
    print os.name   # posix
    print platform.system()
    if "Darwin" == platform.system():
        from get_clang_asan_path import clang_rt_asan_path
        clang_rt_asan_path()
    oldpwd = os.getcwd()
    build_dir = 'build'
    if not os.path.isdir(build_dir):
        os.mkdir(build_dir)
    os.chdir(build_dir)
    cmd = 'cmake ..'
    os.system(cmd)
    os.chdir(oldpwd)

def build():
    oldpwd = os.getcwd()
    build_dir = 'build'
    if not os.path.isdir(build_dir):
        print 'build is not generated properly. please run generate() first!!!'
        return False
    os.chdir(build_dir)
    cmd = 'make'
    os.system(cmd)
    os.chdir(oldpwd)

if __name__ == '__main__':
    # os.chdir(os.path.dirname(os.path.realpath(__file__)))
    generate()
    build()
