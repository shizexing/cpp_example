// this is test program for address sanitizer
// clang >= 3.1 && gcc >= 4.8 support address sanitize
// 
// clang++ -O1 -g -o test -fsanitize=address test_address_sanitizer.cpp && ./test
// clang++ -O1 -g -o test -fsanitize=address -fsanitize-recover=address test_address_sanitizer.cpp && ASAN_OPTIONS=halt_on_error=0 ./test
// gcc -g -o test -fsanitize=address test_address_sanitizer.cpp && ./test
//
// unfortunately, gcc 5.4.0 doesnot support -fsanitize-recover
// gcc -g -o test -fsanitize=address -fsanitize-recover=address test_address_sanitizer.cpp && ASAN_OPTIONS=halt_on_error=0 ./test

#include <stdio.h>

int test_use_after_free(int n) {
    int *array = new int[100];
    delete [] array;
    return array[n];  // BOOM
}

int test_heap_buffer_overflow(int n) {
    int *array = new int[100];
    array[0] = 0;
    int res = array[n + 100];  // BOOM
    delete [] array;
    return res;
}

int test_stack_buffer_overflow(int n) {
    int stack_array[100];
    stack_array[1] = 0;
    return stack_array[n + 100];
}

int global_array[100] = {-1};
int test_global_buffer_overflow(int n) {
    return global_array[n + 100];  // BOOM
}

int main(int argc, char **argv) {
    printf("argc : %d\n", argc);
    int r = 0;

    // use after free
    printf("test use-after-free\n");
    r = test_use_after_free(argc);
    printf("r = %d\n", r);
    // heap buffer overflow
    printf("test heap buffer overflow\n");
    r = test_heap_buffer_overflow(argc);
    printf("r = %d\n", r);
    // stack buffer overflow
    r = test_stack_buffer_overflow(argc);
    printf("r = %d\n", r);
    // global buffer overflow
    r = test_global_buffer_overflow(argc);
    printf("r = %d\n", r);

    return 0;
}

