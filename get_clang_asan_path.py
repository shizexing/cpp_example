#`which python`

import os, subprocess, re

def execute(cmd):
    p = subprocess.Popen('%s' % cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    (output, err) = p.communicate()
    if 0 != p.returncode:
        print 'cmd:', cmd
        print 'ret:', p.returncode
        print 'err:', err
        exit(p.returncode)
    output = output.strip()
    return (p.returncode, output)

def check_existance(fn):
    print fn
    if not os.path.isfile(fn):
        return (False, None)
    f = open(fn)
    lib = None
    ret = False
    for ln in f:
        print ln
        # mo = re.search(r'target_link_libraries[^ ]* +debug +([^\)]*libclang_rt.asan_osx_dynamic.dylib)\)', ln)
        mo = re.search(r'target_link_libraries[^ ]* +([^\)]*libclang_rt.asan_osx_dynamic.dylib)\)', ln)
        if None != mo:
            lib = mo.groups()[0]
            print 'lib:', lib
            ret = True
    return (ret, lib)
    
def clang_rt_asan_path():
    root = os.path.dirname(os.path.realpath(__file__))
    fn = os.path.join(root, 'osx_clang_rt.cmake')
    (r, lib) = check_existance(fn)
    if r:
        return lib
    print fn
    (r, output) = execute('which xcode-select')
    print 'r:', r, ', output:', output
    (r, output) = execute('xcode-select -p')
    print 'r:', r, ', output:', output
    cmd = 'find %s -name "libclang_rt.asan_osx_dynamic.dylib"' % output
    print 'cmd:', cmd
    (r, output) = execute(cmd)
    # output = '/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/lib/clang/9.1.0/lib/darwin/libclang_rt.asan_osx_dynamic.dylib'
    print 'r:', r, ', output:', output
    print 'libname:', os.path.basename(output)
    print 'lib_path:', os.path.dirname(output)

    lib = output
    cmd = 'echo "message(STATUS \\"clang_rt: %s\\")\ntarget_link_libraries(eg_address_sanitize %s)\n" > %s' % (lib, lib, fn)
    print cmd
    (r, output) = execute(cmd)
    print 'r:', r, ', output:', output

    return lib

if __name__ == '__main__':
    p = clang_rt_asan_path()
    print 'xxxx:', p
