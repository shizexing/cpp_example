// /usr/local/gcc7/bin/g++ -o test test_shift_count_overflow.cpp -std=c++11

#include <cstdint>
#include <iostream>

using namespace std;

template <uint32_t Bits, uint32_t Iter=32> struct pack {
    static const uint32_t Mask = (1 << Bits) - 1;
    static const uint32_t Pos = Iter - 1;
    static const uint32_t Start = (Pos * Bits) / 32, End = ((Pos + 1) * Bits - 1) / 32;
    static const uint32_t Offset = 32 - ((Pos + 1) * Bits - End * 32);

    static const uint32_t MaskH = ~(Mask << Offset);
    static const uint32_t MaskL = ~(Mask >> (32 - Offset));
};

template <uint32_t Bits, uint32_t Iter=32>
void test() {
    cout << std::hex;
    cout << "Bits = " << Bits << ", Iter = " << Iter << endl;
    cout << "Mask:" << pack<Bits, Iter>::Mask << endl;
    cout << "Pos:" << pack<Bits, Iter>::Pos << endl;
    cout << "Start:" << pack<Bits, Iter>::Start << endl;
    cout << "End:" << pack<Bits, Iter>::End << endl;
    cout << "Offset:" << pack<Bits, Iter>::Offset << endl;
    cout << "MaskH:" << pack<Bits, Iter>::MaskH << endl;
    cout << "MaskL:" << pack<Bits, Iter>::MaskL << endl;
    cout << endl;
}

int main(int argc, char** argv) {
    test<0>(); 
    test<1>(); 
    test<2>(); 
    test<3>(); 
    test<4>(); 
    test<5>(); 
    test<6>(); 
    test<7>(); 
    test<8>(); 
    test<9>(); 
    test<10>(); 
    test<11>(); 
    test<12>(); 
    test<13>(); 
    test<14>(); 
    test<15>(); 
    test<16>(); 
    test<17>(); 
    test<18>(); 
    test<19>(); 
    test<20>(); 
    test<21>(); 
    test<22>(); 
    test<23>(); 
    test<24>(); 
    test<25>(); 
    test<26>(); 
    test<27>(); 
    test<28>(); 
    test<29>(); 
    test<30>(); 
    test<31>(); 
    test<32>(); 
    return 0;
}

