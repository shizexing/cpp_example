// compile  : gcc -g mtrace_test.cpp -o test -lstdc++
// run      : MALLOC_TRACE=m.log ./test
// check    : mtrace ./test m.log

#include <mcheck.h>
#include <stdlib.h>
#include <stdio.h>
#include <memory>               // new, delete

void usage(char* nm)
{
    printf("usage:\n");
    printf("\t$ MALLOC_TRACE=m.log %s\t#define memory log macro MALLOC_TRACE and run\n", nm);
    printf("\t$ mtrace %s m.log\t\t#run mtrace perl script to analyze memory logs\n\n", nm);
}

int main(int argc, char *argv[])
{
    usage(argv[0]);
    int j;

    mtrace();

    char *c = 0;
    for (j = 0; j < 2; j++) {
        if (c) {
            printf("free %p\n", c);
            free(c);
        }
        c = (char*)malloc(100);            /* Never freed--a memory leak */
        printf("malloc 0x%x bytes: %p\n", 100, c);
    }

    calloc(16, 16);             /* Never freed--a memory leak */

    c = new char[16];
    if (c) {
        delete c;	/* call delete instead of delete[] */
    }
    exit(EXIT_SUCCESS);
}
