// http://man7.org/linux/man-pages/man3/mtrace.3.html
// compile  : gcc -g mtrace_test.c -o test
// run      : MALLOC_TRACE=m.log ./test
// check    : mtrace ./test m.log
#include <mcheck.h>
#include <stdlib.h>
#include <stdio.h>

void usage(char* nm)
{
    printf("usage:\n");
    printf("\t$ MALLOC_TRACE=m.log %s\t#define memory log macro MALLOC_TRACE and run\n", nm);
    printf("\t$ mtrace %s m.log\t\t#run mtrace perl script to analyze memory logs\n\n", nm);
}

int
main(int argc, char *argv[])
{
    usage(argv[0]);

    int j;

    mtrace();

    for (j = 0; j < 2; j++)
        malloc(100);            /* Never freed--a memory leak */

    calloc(16, 16);             /* Never freed--a memory leak */
    exit(EXIT_SUCCESS);
}
